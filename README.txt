Set-up instructions for Secure Chorus KMS:

apt-get -y install git libssl-dev libgmp-dev
apt-get -y install libmojolicious-perl libxml-perl libxml-libxml-perl

# manually set up CPAN
cpan

# use CPAN to install dependencies
cpan install Crypt::OpenSSL::AES Crypt::OpenSSL::ECDSA Crypt::Blowfish Crypt::CBC Crypt::DES_EDE3 Crypt::Rijndael Data::Compare Encoding::BER Math::GMPz Crypt::OpenSSL::Random DBI YAML::XS DBD::SQLite Digest::SHA1 UUID::Tiny

git clone https://bitbucket.org/securechorus/sc_kms
git clone https://bitbucket.org/abutcher/crypt-ecdsa-gmpz.git

# set up and install Crypt::ECDSA
pushd crypt-ecdsa-gmpz
perl Makefile.PL
make
make install
popd

cd sc_kms
morbo sckms.pl
