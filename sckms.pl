#!/usr/bin/env perl

use strict;
use Mojolicious::Lite;
use XML::LibXML;
use Data::Dumper;
use lib 'lib';

require "w3c_xml.pl";
require "kms_xml.pl";
require "kms_db.pl";

# Enable/disable XML sign verifications. Only disable for debug testing
my $verify_xml_signature = 1;

# set mojo default listen address if not running under hypnotoad (i.e.
# running on windows or standalone)
$ENV{MOJO_LISTEN} = 'http://*:7070,https://localhost:7071'  if not $ENV{MOJO_LISTEN};

app->attr(dbh => \&dbconnect);
app->config(hypnotoad => {listen => ['http://*:7070','https://localhost:7071']});

# For the GUI interface
plugin 'auth_helper';
plugin 'basic_auth';

sub render_error
{
  my $mojo = shift;
  my $message = shift;
  my $status = shift;
  
  $mojo->render(data => $message, status => $status);
  return undef;
}

sub render_kms_xml_error
{
  my $mojo = shift;
  my $request = shift;
  my $message = shift;
  my $err_code = shift;
  my $status = shift;
 
  my $doc = create_kms_error_response($request, $message, $err_code);
  print $doc->toString(1); 

  # Check output XML validates
  validate_dom_xml($doc);

  $mojo->render(data => $doc->toString(1), format => 'xml', status => $status);
  return undef;
}

sub render_kms_xml
{
  my $mojo = shift;
  my $doc = shift;

  print STDERR $doc->toString(1); 
  
  # Check output XML validates
  validate_dom_xml($doc);

  $mojo->render(data => $doc->toStringC14N(), format=> 'xml');

  return 1;
}

sub key_prov_common
{
  my $mojo = shift;
  my $response = shift;
  my $trk = shift;
  my $user_uri = shift;
  my $timestamp = shift;
    
  my $dbh = app->dbh;

  # Get key material for user
  my $key_mat = get_key_material($dbh, $response->{'UserUri'}, $user_uri, $timestamp);

  return render_kms_xml_error($mojo, $response,
     'Requested User URI does not correspond to a provisioned URI', 99, 400) if not defined $key_mat;

  # Create reponse and render result
  my $doc = create_kms_key_prov_response($response, $key_mat, $trk);
  return render_kms_xml_error($mojo, $response, 'Internal error', 1, 400) if not $doc;
   
  return render_kms_xml($mojo, $doc);
}

sub cert_cache_common
{
  my $mojo = shift;
  my $response = shift;
  my $trk = shift;
    
  my $dbh = app->dbh;

  my $expire_time = 0xFFFFFFFF;
  my @certs;

  # Get an array of the certificates in the cache
  my $certs = get_all_certs($dbh);

  # Get min expire time
  for my $cert (@{$certs})
  {
    if ($cert->{'ValidTo'} < $expire_time)
    {
      $expire_time = $cert->{'ValidTo'};
    }
  }
  my $cert_token = create_cert_cache_token($dbh, $response->{'UserUri'}, $expire_time);
    
  my $doc = create_kms_cert_cache_response($response, $certs, $trk, $cert_token);
  return render_kms_xml_error($mojo, $response, 'Internal error', 1, 400) if not $doc;

  # Create reponse and render result
  return render_kms_xml($mojo, $doc);
}

group
{
  # KMS Key management common. Get request, verify and extract request data.
  under '/keymanagement/identity/v1' => sub
  {
    my $mojo = shift;

    my $dbh = app->dbh;

    my %request;
    my %response;
    my $rq_dom;

    $response{'Time'} = create_timestamp();
    $response{'KmsId'} = get_kms_id($dbh);
    $response{'ClientReqUrl'} = $mojo->req->url->to_abs;
    $response{'KmsUri'} = get_kms_domain($dbh);
    $response{'UserUri'} = "";

    # POST xml request
    if($mojo->req->headers->content_type =~ m/^text\/xml/)
    {
      print "Received \n" . $mojo->req->body . "\n"; 
      if($mojo->req->body eq "")
      {
        return render_kms_xml_error($mojo, \%response, 'Invalid request - No content', 1, 400);
      }
      $rq_dom = load_validated_xml($mojo->req->body);
    }
    # Check for test POST upload
    elsif($mojo->req->headers->content_type =~ m/^multipart\/form-data/)
    {
      my $file = $mojo->param('myfile');
      $rq_dom = load_validated_xml($file->slurp);
    }
    else
    {
      # Invalid post content type, return NOT ACCEPTABLE
      return render_kms_xml_error($mojo, \%response, 'Post content type not acceptable', 1, 406);
    }

    # Check XML load and validation
    if (!defined($rq_dom))
    {
      return render_kms_xml_error($mojo, \%response, 'Invalid request', 1, 400);
    }

    # Extract content, we need user URI so we can get TrK for user, before we can check signature 
    if(!parse_xml_to_hash($rq_dom, get_kms_namespace(), 'KmsRequest', \%request))
    {
      # This should not happen as XML has been validated, return BAD REQUEST
      return render_kms_xml_error($mojo, \%response, 'Invalid request - XML parse error', 1, 400);
    }
    
    # Set User URI in response
    $response{'UserUri'} = $request{'UserUri'};

    # Check request is for our KMS
    if (!defined check_kms_domain($dbh, $request{'KmsUri'}))
    {
      return render_kms_xml_error($mojo, \%response, 'KMS URI Domain not recognised', 18, 400);
    }

    # Lookup TrK for user (also checks user exists).
    my $trk = lookup_trk_by_user_uri($dbh, $request{'UserUri'});
    if (!defined($trk))
    {
      return render_kms_xml_error($mojo, \%response,
        'Requested User URI does not correspond to a provisioned URI', 99, 400);
    }

    # Check if it is signed correctly
    if ($verify_xml_signature)
    {
      if (!verify_xml($rq_dom, get_kms_namespace(), $trk->{'Key'}, $trk->{'KeyName'}))
      {
        # Request signature is bad, return HTTP_BAD_REQUEST 
        return render_kms_xml_error($mojo, \%response, 'HMAC Signature does not verify', 35, 400);
      }
    }

    # Stash a reference to the response fields hash and the signing trk
    $mojo->stash('response' => \%response);
    $mojo->stash('trk' => $trk);

    return 1;
  };

  # KMS Initialise Request
  post '/init' => sub
  {
    my $mojo = shift;
    my $response = $mojo->stash('response');
    my $old_trk = $mojo->stash('trk');

    my $dbh = app->dbh;

    # Obtain the current root certificate
    my $cert = get_root_cert($dbh);

    # Create an new TrK for user and add to database
    my $new_trk = create_trk($dbh, $response->{'UserUri'});

    # Create reponse and render result
    my $doc = create_kms_init_response($response, $cert, $old_trk, $new_trk);
    return render_kms_xml_error($mojo, $response, 'Internal error', 1, 400) if not $doc;

    if (!render_kms_xml($mojo, $doc))
    {
      return render_kms_xml_error($mojo, $response, 'Internal error', 1, 400);
    }

    # Update user URI to new key and remove old key
    if (update_account_trk_from_uri($dbh, $new_trk->{'KeyName'}, $response->{'UserUri'}))
    {
      remove_trk($dbh, $old_trk->{'KeyName'});
    }
    else
    {
      # Internal error
      return render_kms_xml_error($mojo, $response, 'Internal database error', 1, 400);
    }
  
    return 1;
  };

  # KMS Key Provision Request. Gets keys for user URI and all psuedonums
  post '/keyprov' => sub
  {
    my $mojo = shift;
    my $response = $mojo->stash('response');
    my $trk = $mojo->stash('trk');

    my $user_uri = $response->{'UserUri'}; 
    return key_prov_common($mojo, $response, $trk, undef, undef);
  };

  # KMS Key Provision Request. Gets keys for specific user psuedomum
  # Note use of relaxed placeholder as domain contains a dot
  post '/keyprov/#useruri' => sub
  {
    my $mojo = shift;
    my $response = $mojo->stash('response');
    my $trk = $mojo->stash('trk');
    my $user_uri = $mojo->param('useruri');

    return key_prov_common($mojo, $response, $trk, $user_uri, undef);
  };
  
  # KMS Key Provision Request. Gets keys for specific user psuedomum at specfic time
  post '/keyprov/#useruri/#timestamp' => sub
  {
    my $mojo = shift;
    my $response = $mojo->stash('response');
    my $trk = $mojo->stash('trk');
    my $user_uri = $mojo->param('useruri');
    my $timestamp = parse_ntp_timestamp($mojo->param('timestamp'));

    return render_error($mojo, 'Invalid request timestamp', 400, 1) if not $timestamp;
    return key_prov_common($mojo, $response, $trk, $user_uri, $timestamp->{'epoch'});
  };
    
  # KMS Certificate cache provision request. Gets all certificates in the cache.
  post '/certcache' => sub
  {
    my $mojo = shift;
    my $response = $mojo->stash('response');
    my $trk = $mojo->stash('trk');

    return cert_cache_common($mojo, $response, $trk);
  };

  # KMS Certificate cache provision request. Gets changes to a specified cache.
  post '/certcache/:token' => sub
  {
    my $mojo = shift;
    my $response = $mojo->stash('response');
    my $trk = $mojo->stash('trk');
    my $token = $mojo->param('token');
    
    my $dbh = app->dbh;
    my $token_status = get_cert_cache_token($dbh, $response->{'UserUri'}, $token);

    if ($token_status == 1)
    {
      # Cache upto date
      my @certs; # Empty array
      my $doc = create_kms_cert_cache_response($response, \@certs, $trk);

      if (!render_kms_xml($mojo, $doc))
      {
        return render_kms_xml_error($mojo, $response, 'Internal error', 1, 400);
      }
    }
    elsif ($token_status == 2)
    {
      # Cache token has expired resend new cache set
      return cert_cache_common($mojo, $response, $trk);
    }
    else
    {
      # Its an error
      return render_kms_xml_error($mojo, $response, 'Invalid cache token', 1, 400);
    }
  };
};

################################# GUI Interface ############################
# This is from the orignal MS KMS developed SELEX Elsag

# These functions are needed in this file
sub add_account_uri { db_add_account_uri(app->dbh, @_ ) }
sub update_account_uri { db_add_account_uri(app->dbh, @_ ) }
sub remove_account_uri { db_remove_account_uri(app->dbh, @_ ) }

sub render_page
{
   my $mojo = shift;
   my $page = shift;
   my $accountId = get_account_from_sid(app->dbh, $mojo->session('sid'));

   my ($user, $admin) = app->dbh->selectrow_array(
      "SELECT name, admin FROM accounts WHERE id = $accountId");
   return $mojo->render($page, (uid => $accountId, user => $user, admin => $admin));
}

sub render_json_error
{
   my $mojo = shift;
   $mojo->render('json' => json_error(@_));
}

group
{
   under '/secure' => sub
   {
      my $mojo = shift;
      return 1 if $mojo->check_auth()->{authorized};

      # support direct access via basic-auth for /key subpath
      if ((my $sub = $mojo->req->url->path->parts->[1]))
      {
         if ($sub eq '1') # REST API 1 supports session creation via GET /secure/1/session
         {
            if ($mojo->req->url->path->parts->[2] eq 'session')
            {
               return 1 if $mojo->req->method eq 'GET';
               render_json_error $mojo, "Not authenticated, no session to delete.", 401;
               return 0;
            }
         }
      }
      $mojo->session('post-login-url' => $mojo->req->url->path->to_abs_string);
      $mojo->flash(error => "401:Authentication required.");
      $mojo->redirect_to('/login'); # re-direct to here when not logged in.
      return 0;
   };

   get '/' => sub
   {
      render_page shift, 'main';
   };

   group { under '1'; ## version 1 REST API group

   get '/user' => sub
   {
      my $mojo = shift;
      my $accountId = get_account_from_sid(app->dbh, $mojo->session('sid'));

      my $where_clause = '';
      $where_clause = "WHERE id = $accountId" if $mojo->param('restrict');
      my $users = app->dbh->selectall_arrayref(
         "SELECT id, name FROM accounts $where_clause", {Slice => {}});
      return $mojo->render(json => $users);
   };
   
   post '/user' => sub
   {
      my $mojo = shift;
      my $dbh = app->dbh;

      my $accountId = get_account_from_sid($dbh, $mojo->session('sid'));
      my $name = $mojo->req->json('/name');
      my $pass = $mojo->req->json('/pass');

      return render_json_error $mojo, "Both 'user' and 'pass' must be provided and non-empty.", 400
         if not $name or not $pass;
      return $mojo->render('json' => add_user($dbh, $accountId, $name, $pass));
   };

   any [qw(POST GET PUT DELETE)] => '/user/:name_or_uid/uri' => sub
   {
      my $dbh = app->dbh;
      my $mojo = shift;

      my $name_or_uid = $mojo->param('name_or_uid');
      my $subjectId = resolve_to_account($dbh, $name_or_uid);

      return render_json_error $mojo, "No such account '$name_or_uid'", 404
         if not defined $subjectId;

      my $method = $mojo->req->method;

      if ($method eq 'GET')
      {
         my $uris = app->dbh->selectall_arrayref(
            "SELECT uri FROM account_uris WHERE id = $subjectId", {Slice => {}}) ;
         return $mojo->render('json' => $uris);
      }

      my $accountId = get_account_from_sid($dbh, $mojo->session('sid'));

      return render_json_error $mojo, "Only the account owner or an administrator may add manage URIs.", 403
         if $subjectId ne $accountId and not is_admin_account($dbh, $accountId);

      my $body_uri = $mojo->req->json('/uri');
      my $query_uri = $mojo->req->param('uri');

      return $mojo->render('json' => add_account_uri $subjectId, $body_uri) if $method eq 'POST';
      return $mojo->render('json' => update_account_uri $subjectId, $query_uri, $body_uri) if $method eq 'PUT';
      return $mojo->render('json' => remove_account_uri $subjectId, $query_uri) if $method eq 'DELETE';
   };
   }; # version 1 REST API group
};

get '/login' => sub
{
   my $mojo = shift;
   my $error = $mojo->flash('error');
   if ($error and $error =~ m/^([0-9]+):(.*)$/)
   {
      $mojo->res->code($1);
      $error = $2;
   }
   $mojo->render('main', (login => 1, error => $error));
};

post '/login' => sub
{
   my $mojo = shift;

   my $user = $mojo->login($mojo->param('username'), $mojo->param('password'));

   if ($user->{authorized})
   {
      my $post_login_url = $mojo->session('post-login-url') || '/';
      delete $mojo->session->{'post-login-url'};
      return $mojo->redirect_to($post_login_url);
   }

   $mojo->flash(error => "401:Failed to authenticate.");
   $mojo->redirect_to('/login');
};

any ['get', 'post'] => '/logout' => sub
{
   my $mojo = shift;
   $mojo->logout($mojo->param('all-sessions'));
   $mojo->redirect_to('/');
};

get '/ui/:page' => sub
{
   my $mojo = shift;
   render_page $mojo, $mojo->param('page');
};

get '/' => sub
{
   my $mojo = shift;
   return $mojo->redirect_to('/login') if not $mojo->check_auth()->{authorized};
   return $mojo->redirect_to('/secure/')
};

# Start HTTP server
app->start;
